/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas_Administrador;

import Vistas_Inició.VentanaPrincipal;
import Controladores_bd_sql.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.Aerolinea;

/**
 *
 * @author FM_Alva
 */
public class VentanaActualizarAerolinea extends javax.swing.JFrame {
    //VARIABLES GLOVALES
    private DefaultTableModel modeloTabla = new DefaultTableModel();
    static Aerolinea aerolinea;
    
    //CONTRUSCTORES

    public VentanaActualizarAerolinea() {
        
        agregarModeloTabla();
        listaAerolineas();
        initComponents();
        cajaId.setVisible(false);
        //OCULTAMOS LA COLUMNA ID
        tabla.getColumnModel().getColumn(6).setMaxWidth(0);
        tabla.getColumnModel().getColumn(6).setMinWidth(0);
        tabla.getColumnModel().getColumn(6).setPreferredWidth(0);
        setLocationRelativeTo(null);
        
   
        
    }
    
   
    
    public VentanaActualizarAerolinea(Aerolinea aerolinea) {
        
        agregarModeloTabla();
        listaAerolineas();
        initComponents();
       
        cajaId.setVisible(false);
        this.aerolinea = aerolinea;
        //OCULTAMOS LA COLUMNA ID
        tabla.getColumnModel().getColumn(6).setMaxWidth(0);
        tabla.getColumnModel().getColumn(6).setMinWidth(0);
        tabla.getColumnModel().getColumn(6).setPreferredWidth(0);
        setLocationRelativeTo(null);
        
         
        
    }

    
    //METODO QUE DA FORMATO A LA TABLA
    private void agregarModeloTabla(){
        modeloTabla.addColumn("Aerolinea");
        modeloTabla.addColumn("Encargado");
        modeloTabla.addColumn("Apellidos");
        modeloTabla.addColumn("Dni");
        modeloTabla.addColumn("Teléfono");
        modeloTabla.addColumn("Correo");
        modeloTabla.addColumn("id");
    }
    
    //METODO QUE MUESTRA LA LISTA DE AEROLINEAS EN LA TABLA
    public boolean listaAerolineas(){
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
   
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select * from aerolinea where id_tipousuario=2");
            rs = ps.executeQuery();
            
                while(rs.next()){
                 modeloTabla.addRow(new Object[]{rs.getString("nombre_aerolinea"),rs.getString("nombre_encargado"),rs.getString("apellidos"),rs.getString("dni"),rs.getString("telefono"),rs.getString("correo"),rs.getInt("id_aerolinea")});
                    
                }
                
                
            //conexion.close();
            return false; //
            
                }catch(Exception ex){
                    return false;
                   
                }
            
           
    }
    
    private void limpiarTabla(){
        for (int i = 0; i < tabla.getRowCount(); i++) {
        modeloTabla.removeRow(i);
        i-=1;
        }
    }
    
    public boolean modificarAerolinea(){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("update aerolinea set nombre_aerolinea=?,nombre_encargado=?,apellidos=?,dni=?,telefono=?,correo=? where id_aerolinea=? ");
            ps.setString(1,cajaNombreAerolinea.getText());
            ps.setString(2,cajaNombreEncargado.getText());
            ps.setString(3,cajaApellidos.getText());
            ps.setString(4,cajaDni.getText());
            ps.setString(5,cajaTelefono.getText());
            ps.setString(6,cajaCorreo.getText());
            ps.setInt(7, Integer.parseInt(cajaId.getText()));
           
   
                
            int resultado =ps.executeUpdate();
            
            
                if(resultado >0){
                     JOptionPane.showMessageDialog(null,"Registro actualizado correctamente");
                      limpiarCajas();
                      return true;
                
                
                }else{
                     JOptionPane.showMessageDialog(null,"Error al actualizar registro");
                     return false;
                 }
            
            
            }catch(Exception ex){
                System.err.println("Error"+ ex);
                return false;
                
            }
    }
    public boolean eliminarAerolinea(){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("delete from aerolinea where id_aerolinea=?");
             ps.setInt(1, Integer.parseInt(cajaId.getText()));
              
            int resultado =ps.executeUpdate();
            
            
                if(resultado >0){
                     JOptionPane.showMessageDialog(null,"Registro eliminado correctamente");
                     limpiarCajas();
                     return true;
                
                
                }else{
                     JOptionPane.showMessageDialog(null,"Error al eliminar registro");
                     return false;
                 }
            
            
            }catch(Exception ex){
                System.err.println("Error"+ ex);
                return false;
                
            }
    }
    
     public void limpiarCajas() {
        cajaNombreAerolinea.setText("");
        cajaNombreEncargado.setText("");
        cajaApellidos.setText("");
        cajaDni.setText("");
        cajaTelefono.setText("");
        cajaCorreo.setText("");
        cajaId.setText("");
    }
     
    
      
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        etiquetaAerolinea = new javax.swing.JLabel();
        scrolTabla = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        etiquetaDatos = new javax.swing.JLabel();
        etiquetaNombreAerolinea = new javax.swing.JLabel();
        etiquetaNombreEncargado = new javax.swing.JLabel();
        etiquetaNombreApellidos = new javax.swing.JLabel();
        etiquetaDni = new javax.swing.JLabel();
        etiquetaTelefono = new javax.swing.JLabel();
        etiquetaCorreo = new javax.swing.JLabel();
        cajaNombreAerolinea = new javax.swing.JTextField();
        cajaNombreEncargado = new javax.swing.JTextField();
        botonModificar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonAtras = new javax.swing.JButton();
        cajaDni = new javax.swing.JTextField();
        cajaApellidos = new javax.swing.JTextField();
        cajaCorreo = new javax.swing.JTextField();
        cajaTelefono = new javax.swing.JTextField();
        cajaId = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Administrar Aerolíneas");
        setResizable(false);

        jPanel1.setPreferredSize(new java.awt.Dimension(526, 520));

        etiquetaAerolinea.setFont(new java.awt.Font("Arabic Typesetting", 1, 36)); // NOI18N
        etiquetaAerolinea.setText("AEROLÍNEAS");

        tabla.setModel(modeloTabla);
        ListSelectionListener oyenteSeleccion = new ListSelectionListener(){
            @Override
            //listSelectionListener oyente de selecion multiple
            public void valueChanged (ListSelectionEvent e){

                //le decimos que el objeto listener, si se ejecuta mas de una vez solo lo haga 1
                if(e.getValueIsAdjusting()){//
                    //OBETENEMOS LA FILA SELECCIONADA
                    int filaSeleccionada = tabla.getSelectedRow();

                    /*
                    GETVALUE METODO QUE DEVUELVE EL VALOR SELECCIONADO, LE TENDREMOS QUE PASAR
                    LA FILA Y COLUMNA, ESTO DEVOLVERA UN OBJETO, LO PASAMOS A STRING PARA PODER
                    MOSTRARLO
                    */

                    String nombreAerolinea = (String)modeloTabla.getValueAt(filaSeleccionada, 0); 
                    String nombreEncargado = (String)modeloTabla.getValueAt(filaSeleccionada, 1);
                    String apellidos = (String)modeloTabla.getValueAt(filaSeleccionada, 2);
                    String dni = (String)modeloTabla.getValueAt(filaSeleccionada, 3);
                    String telefono = (String)modeloTabla.getValueAt(filaSeleccionada, 4);
                    String correo = (String)modeloTabla.getValueAt(filaSeleccionada, 5);
                    int id = (int)modeloTabla.getValueAt(filaSeleccionada, 6);

                    String id1 = Integer.toString(id);

                    cajaNombreAerolinea.setText(nombreAerolinea);
                    cajaNombreEncargado.setText(nombreEncargado);
                    cajaApellidos.setText(apellidos);
                    cajaDni.setText(dni);
                    cajaTelefono.setText(telefono);
                    cajaCorreo.setText(correo);
                    cajaId.setText(id1);
                }
            }
        };
        //
        tabla.getSelectionModel().addListSelectionListener(oyenteSeleccion);
        scrolTabla.setViewportView(tabla);

        etiquetaDatos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDatos.setText("Datos Aerolinea");

        etiquetaNombreAerolinea.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombreAerolinea.setText("Nombre Aerolinea ");

        etiquetaNombreEncargado.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombreEncargado.setText("Nombre Encargado");

        etiquetaNombreApellidos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombreApellidos.setText("Apellidos");

        etiquetaDni.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDni.setText("Dni");

        etiquetaTelefono.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaTelefono.setText("Teléfono");

        etiquetaCorreo.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaCorreo.setText("Correo");

        cajaNombreAerolinea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaNombreAerolineaActionPerformed(evt);
            }
        });

        botonModificar.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonModificar.setText("Modificar");
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonEliminar.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonEliminar.setText("Eliminar");
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonAtras.setText("Atras");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        cajaApellidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaApellidosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(botonAtras)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(botonModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonEliminar)
                .addGap(63, 63, 63))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrolTabla)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiquetaCorreo)
                    .addComponent(etiquetaDatos)
                    .addComponent(etiquetaDni)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cajaDni, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaNombreAerolinea, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cajaNombreAerolinea, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(69, 69, 69)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(etiquetaTelefono)
                            .addComponent(cajaNombreEncargado, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaNombreEncargado)
                            .addComponent(etiquetaNombreApellidos)
                            .addComponent(cajaApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cajaTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(etiquetaAerolinea)
                    .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(etiquetaAerolinea)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrolTabla, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etiquetaDatos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaNombreAerolinea)
                    .addComponent(etiquetaNombreEncargado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cajaNombreEncargado, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addComponent(cajaNombreAerolinea))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaDni)
                    .addComponent(etiquetaNombreApellidos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cajaApellidos, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(cajaDni))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaCorreo)
                    .addComponent(etiquetaTelefono))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cajaTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(cajaCorreo))
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonEliminar)
                    .addComponent(botonModificar))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonAtras)
                    .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed
        VentanaPrincipal v1 = new VentanaPrincipal(aerolinea);
        v1.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_botonAtrasActionPerformed

    private void cajaNombreAerolineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaNombreAerolineaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cajaNombreAerolineaActionPerformed

    private void cajaApellidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaApellidosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cajaApellidosActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
     if (cajaNombreAerolinea.getText().equals("")  || cajaNombreEncargado.getText().equals("")|| cajaApellidos.getText().equals("") || cajaDni.getText().equals("")
     || cajaTelefono.getText().equals("") || cajaCorreo.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Porfavor Rellene todos los campos");
     }else{
        modificarAerolinea();
        limpiarTabla();
        listaAerolineas();
        
     }
      
     
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
      eliminarAerolinea();
      limpiarTabla();
      listaAerolineas();
        
    }//GEN-LAST:event_botonEliminarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
       
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
    
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaActualizarAerolinea().setVisible(true);
                
        
      
            }
        });
        
         try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAtras;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JTextField cajaApellidos;
    private javax.swing.JTextField cajaCorreo;
    private javax.swing.JTextField cajaDni;
    private javax.swing.JTextField cajaId;
    private javax.swing.JTextField cajaNombreAerolinea;
    private javax.swing.JTextField cajaNombreEncargado;
    private javax.swing.JTextField cajaTelefono;
    private javax.swing.JLabel etiquetaAerolinea;
    private javax.swing.JLabel etiquetaCorreo;
    private javax.swing.JLabel etiquetaDatos;
    private javax.swing.JLabel etiquetaDni;
    private javax.swing.JLabel etiquetaNombreAerolinea;
    private javax.swing.JLabel etiquetaNombreApellidos;
    private javax.swing.JLabel etiquetaNombreEncargado;
    private javax.swing.JLabel etiquetaTelefono;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JScrollPane scrolTabla;
    public javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables
}
