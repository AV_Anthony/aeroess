package Vistas_GestionAerolinea;

import Vistas_Inició.VentanaPrincipal;
import Controladores_bd_sql.Conexion;
import Controladores_bd_sql.SQLAeroess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;

import java.text.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import modelo.Aerolinea;
import modelo.Vuelo;

/**
 *
 * @author FM_Alva
 */
public class VentanaActualizarVuelo extends javax.swing.JFrame {

    Aerolinea aerolinea = new Aerolinea();
    Vuelo vuelo = new Vuelo();
    SQLAeroess sqlAeroess = new SQLAeroess();
    private DefaultTableModel modeloTabla = new DefaultTableModel();
    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

    public VentanaActualizarVuelo() {
        agregarModeloTabla();
        listaAerolineas();
        initComponents();
        formatoJSpiner();
        //cajaId.setVisible(false);
        setLocationRelativeTo(null);
    }

    public VentanaActualizarVuelo(Aerolinea aerolinea) {
        this.aerolinea = aerolinea;
        agregarModeloTabla();
        listaAerolineas();

        initComponents();
        formatoJSpiner();
        
        cajaId.setVisible(false);
        //OCULTAMOS LA COLUMNA ID
       
        //OCULTAMOS LA COLUMNA ID
       
        tabla.getColumnModel().getColumn(9).setMaxWidth(0);
        tabla.getColumnModel().getColumn(9).setMinWidth(0);
        tabla.getColumnModel().getColumn(9).setPreferredWidth(0);
        
        tabla.getColumnModel().getColumn(8).setMaxWidth(0);
        tabla.getColumnModel().getColumn(8).setMinWidth(0);
        tabla.getColumnModel().getColumn(8).setPreferredWidth(0);
        
        tabla.getColumnModel().getColumn(7).setMaxWidth(0);
        tabla.getColumnModel().getColumn(7).setMinWidth(0);
        tabla.getColumnModel().getColumn(7).setPreferredWidth(0);
      
        setLocationRelativeTo(null);
    }

    private void agregarModeloTabla() {
        modeloTabla.addColumn("Numero Vuelo"); 
        modeloTabla.addColumn("Origen");
        modeloTabla.addColumn("Destino");
        modeloTabla.addColumn("Fecha salida");
        modeloTabla.addColumn("Hora salida");
        modeloTabla.addColumn("Fecha Llegada");
        modeloTabla.addColumn("Hora Llegada");
        modeloTabla.addColumn("Numero pasajeros");
        modeloTabla.addColumn("Precio");
        modeloTabla.addColumn("ID");
    }
    
     private void limpiarTabla(){
        for (int i = 0; i < tabla.getRowCount(); i++) {
        modeloTabla.removeRow(i);
        i-=1;
        }
    }

    public boolean listaAerolineas() {
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Connection conexion = con.getConnection();

            ps = conexion.prepareStatement("select * from vuelo where id_aerolineaFk=?");
            ps.setInt(1, aerolinea.getId());

            rs = ps.executeQuery();

            while (rs.next()) {
                modeloTabla.addRow(new Object[]{rs.getString("numero_vuelo"),rs.getString("origen"), rs.getString("destino"), rs.getString("fecha_salida"),rs.getString("hora_salida"), rs.getString("fecha_llegada"), rs.getString("hora_llegada"), rs.getInt("numMax_pasajeros"),rs.getInt("precio"), rs.getInt("id_vuelo") });
               
            }
   
            conexion.close();
            return false; //

        } catch (Exception ex) {
            System.err.println(ex);
            
            return false;

        }
        
    }
    public void limpiarCajas(){
        cajaNumeroVuelo.setText("");
        cajaPrecio.setText("");
        cajaOrigen.setText("");
        cajaDestino.setText("");
        cajaNumeroPasajeros.setText("");
        cajaId.setText("");
        cajaBuscar.setText("");
    }
    
     public void formatoJSpiner(){
        
        JSpinner.DateEditor de1 = new JSpinner.DateEditor(spinerFechaLlegada, "dd.MM.yyyy" );
        spinerFechaLlegada.setEditor(de1);
        JSpinner.DateEditor de2 = new JSpinner.DateEditor(spinerFechaSalida, "dd.MM.yyyy" );
        spinerFechaSalida.setEditor(de2);
        
        
        JSpinner.DateEditor d3 = new JSpinner.DateEditor(spinerHoraLlegada, "HH:mm:ss");
        spinerHoraLlegada.setEditor(d3);
        JSpinner.DateEditor d4 = new JSpinner.DateEditor(spinerHoraSalida, "HH:mm:ss");
        spinerHoraSalida.setEditor(d4);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        etiquetaRegistro = new javax.swing.JLabel();
        etiquetaDestino = new javax.swing.JLabel();
        etiquetaOrigen = new javax.swing.JLabel();
        etiquetaFechaSalida = new javax.swing.JLabel();
        etiquetaNumeroVuelo = new javax.swing.JLabel();
        etiquetaFechaLlegada = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        etiquetaNumeroPasajeros = new javax.swing.JLabel();
        cajaNumeroPasajeros = new javax.swing.JTextField();
        botonAtras = new javax.swing.JButton();
        etiquetaPrecio = new javax.swing.JLabel();
        botonEliminar = new javax.swing.JButton();
        botonBuscar = new javax.swing.JButton();
        cajaBuscar = new javax.swing.JTextField();
        botonModificar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        etiquetaDatos = new javax.swing.JLabel();
        cajaNumeroVuelo = new javax.swing.JTextField();
        cajaPrecio = new javax.swing.JTextField();
        cajaOrigen = new javax.swing.JTextField();
        cajaDestino = new javax.swing.JTextField();
        cajaId = new javax.swing.JTextField();
        spinerFechaSalida = new javax.swing.JSpinner(new SpinnerDateModel());
        spinerHoraSalida = new javax.swing.JSpinner(new SpinnerDateModel());
        spinerFechaLlegada = new javax.swing.JSpinner(new SpinnerDateModel());
        spinerHoraLlegada = new javax.swing.JSpinner(new SpinnerDateModel());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Administrar Vuelos");
        setResizable(false);

        jPanel1.setToolTipText("");

        etiquetaRegistro.setFont(new java.awt.Font("Arabic Typesetting", 1, 36)); // NOI18N
        etiquetaRegistro.setText(" VUELOS");

        etiquetaDestino.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDestino.setText("Destino:");

        etiquetaOrigen.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaOrigen.setText("Origen:");

        etiquetaFechaSalida.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaFechaSalida.setText("Fecha de Salida:");

        etiquetaNumeroVuelo.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNumeroVuelo.setText("Numero de vuelo:");

        etiquetaFechaLlegada.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaFechaLlegada.setText("Fecha de Llegada:");

        etiquetaNumeroPasajeros.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNumeroPasajeros.setText("<html>Numero Maximo de Pasajeros:</html>");

        botonAtras.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonAtras.setText("atras");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        etiquetaPrecio.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaPrecio.setText("Precio:");

        botonEliminar.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonEliminar.setText("Eliminar");
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonBuscar.setText("Buscar");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        botonModificar.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonModificar.setText("Modificar");
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        tabla.setModel(modeloTabla);
        ListSelectionListener oyenteSeleccion = new ListSelectionListener(){
            @Override
            //listSelectionListener oyente de selecion multiple
            public void valueChanged (ListSelectionEvent e){

                //le decimos que el objeto listener, si se ejecuta mas de una vez solo lo haga 1
                if(e.getValueIsAdjusting()){//
                    //OBETENEMOS LA FILA SELECCIONADA
                    int filaSeleccionada = tabla.getSelectedRow();

                    /*
                    GETVALUE METODO QUE DEVUELVE EL VALOR SELECCIONADO, LE TENDREMOS QUE PASAR
                    LA FILA Y COLUMNA, ESTO DEVOLVERA UN OBJETO, LO PASAMOS A STRING PARA PODER
                    MOSTRARLO
                    */

                    String numeroVuelo = (String)modeloTabla.getValueAt(filaSeleccionada, 0); 
                    String origen = (String)modeloTabla.getValueAt(filaSeleccionada, 1);
                    String destino = (String)modeloTabla.getValueAt(filaSeleccionada, 2);
                    String fechaSali=  (String)modeloTabla.getValueAt(filaSeleccionada, 3);
                    String horaSali = (String)modeloTabla.getValueAt(filaSeleccionada, 4);
                    String fechaLle= (String)modeloTabla.getValueAt(filaSeleccionada, 5);
                    String horaLle = (String)modeloTabla.getValueAt(filaSeleccionada, 6);
                    int nPasajeros = (int)modeloTabla.getValueAt(filaSeleccionada, 7);
                    int precio = (int)modeloTabla.getValueAt(filaSeleccionada, 8);
                    int id = (int)modeloTabla.getValueAt(filaSeleccionada, 9);

                    String id1 = Integer.toString(id);
                    String precio1 = Integer.toString(precio);
                    String numeroPasajeros= Integer.toString(nPasajeros);

                    Date fechaSalida = null;
                    Date fechaLlegada = null;
                    Date horaSalida = null;
                    Date horaLlegada = null;

                    try {
                        fechaSalida = formatoFecha.parse(fechaSali);
                        fechaLlegada = formatoFecha.parse(fechaLle);
                        horaSalida = formatoHora.parse(horaSali);
                        horaLlegada = formatoHora.parse(horaLle);

                    } 
                    catch (ParseException ex) 
                    {
                        System.out.println("Error formato Jspiner"+ex);
                    }

                    cajaNumeroVuelo.setText(numeroVuelo);

                    cajaPrecio.setText(precio1);
                    cajaOrigen.setText(origen);
                    cajaDestino.setText(destino);
                    spinerFechaSalida.setValue(fechaSalida);
                    spinerHoraSalida.setValue(horaSalida);
                    spinerFechaLlegada.setValue(fechaLlegada);
                    spinerHoraLlegada.setValue(horaLlegada);
                    cajaNumeroPasajeros.setText(numeroPasajeros);
                    cajaId.setText(id1);   
                }
            }
        };
        //
        tabla.getSelectionModel().addListSelectionListener(oyenteSeleccion);
        jScrollPane1.setViewportView(tabla);

        etiquetaDatos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDatos.setText("Datos del Vuelo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(etiquetaRegistro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cajaBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonBuscar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(botonAtras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonModificar)
                        .addGap(113, 113, 113)
                        .addComponent(botonEliminar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(etiquetaNumeroVuelo)
                            .addComponent(cajaNumeroPasajeros, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaNumeroPasajeros, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaOrigen)
                            .addComponent(etiquetaDatos))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(etiquetaPrecio)
                                .addGap(174, 174, 174))
                            .addComponent(etiquetaDestino)
                            .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(etiquetaFechaSalida)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(spinerFechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(5, 5, 5)
                                    .addComponent(spinerHoraSalida))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cajaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cajaNumeroVuelo, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(etiquetaFechaLlegada)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cajaPrecio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cajaDestino, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(spinerFechaLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(spinerHoraLlegada))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etiquetaDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaNumeroVuelo)
                    .addComponent(etiquetaPrecio))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cajaPrecio)
                    .addComponent(cajaNumeroVuelo, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaOrigen)
                    .addComponent(etiquetaDestino))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cajaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaFechaSalida)
                    .addComponent(etiquetaFechaLlegada))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spinerHoraSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinerFechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinerFechaLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinerHoraLlegada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(etiquetaNumeroPasajeros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cajaNumeroPasajeros, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(botonAtras)
                            .addComponent(botonModificar)
                            .addComponent(botonEliminar)))
                    .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed
        VentanaPrincipal v1 = new VentanaPrincipal(aerolinea);
        v1.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_botonAtrasActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        if (cajaNumeroVuelo.getText().equals("") || cajaPrecio.getText().equals("") || cajaDestino.getText().equals("") || cajaOrigen.getText().equals("") || cajaNumeroPasajeros.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Rellene todos los campos");

        } else {
            
       //OBTENEMOS EL VALOR DE LOS JSPINERS, HACEMOS UN CASTING
        String fechaSalidaObtenida = formatoFecha.format(spinerFechaSalida.getValue());
        String fechaLlegadaObtenida = formatoFecha.format(spinerFechaLlegada.getValue());
        String horaSalidaObtenida = formatoHora.format(spinerHoraSalida.getValue());
        String horaLlegadaObtenida = formatoHora.format(spinerHoraLlegada.getValue());
            
            vuelo.setId(Integer.parseInt(cajaId.getText()));
            vuelo.setNumeroVuelo(cajaNumeroVuelo.getText());
            vuelo.setPrecio(Integer.parseInt(cajaPrecio.getText()));
            vuelo.setDestino(cajaDestino.getText());
            vuelo.setOrigen(cajaOrigen.getText());
            vuelo.setFechaSalida(fechaSalidaObtenida);
            vuelo.setHoraSalida(horaSalidaObtenida);
            vuelo.setFechaLlegada(fechaLlegadaObtenida);
            vuelo.setHorsaLlegada(horaLlegadaObtenida);
            vuelo.setNumeroMaximoPasajeros(Integer.parseInt(cajaNumeroPasajeros.getText()));

            if (sqlAeroess.modificarVuelo(vuelo)) {
                limpiarCajas();
                limpiarTabla();
                listaAerolineas();
            }

        }
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
       vuelo.setId(Integer.parseInt(cajaId.getText()));
       sqlAeroess.eliminarVuelo(vuelo);
       limpiarCajas();
       limpiarTabla();
       listaAerolineas();
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        
        Vuelo vuelo1= null;
        vuelo1 =sqlAeroess.buscarVueloAerloniea(cajaBuscar.getText());
         if(vuelo1 == null){
             
         }else{
         limpiarTabla();
         limpiarCajas();
         modeloTabla.addRow(new Object[]{vuelo1.getNumeroVuelo(),vuelo1.getOrigen(),vuelo1.getDestino(),vuelo1.getFechaSalida(),vuelo1.getHoraSalida(),vuelo1.getFechaLlegada(),vuelo1.getHorsaLlegada(),vuelo1.getNumeroMaximoPasajeros(),vuelo1.getPrecio(),vuelo1.getId()});
         }
       
        
        
        
      
    
    }//GEN-LAST:event_botonBuscarActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarVuelo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarVuelo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarVuelo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaActualizarVuelo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*ESTILO
        Estilo de netBeans
            ESTILO1: javax.swing.plaf.metal.MetalLookAndFeel
            ESTILOS DESCARGADOS:
             //ESTILOS CLAROS 
            com.jtattoo.plaf.aero.AeroLookAndFeel  -->MEJOR
            com.jtattoo.plaf.acryl.AcryLookAndFeel
            //ESTILOS NEGROS
            com.jtattoo.plaf.graphite.GraphiteLookAndFeel
            
         */
        // -->asignacion de estilo
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
        } catch (ClassNotFoundException ex) {
            //  Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaActualizarVuelo().setVisible(true);
            }
        });
        
     
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAtras;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JTextField cajaBuscar;
    private javax.swing.JTextField cajaDestino;
    private javax.swing.JTextField cajaId;
    private javax.swing.JTextField cajaNumeroPasajeros;
    private javax.swing.JTextField cajaNumeroVuelo;
    private javax.swing.JTextField cajaOrigen;
    private javax.swing.JTextField cajaPrecio;
    private javax.swing.JLabel etiquetaDatos;
    private javax.swing.JLabel etiquetaDestino;
    private javax.swing.JLabel etiquetaFechaLlegada;
    private javax.swing.JLabel etiquetaFechaSalida;
    private javax.swing.JLabel etiquetaNumeroPasajeros;
    private javax.swing.JLabel etiquetaNumeroVuelo;
    private javax.swing.JLabel etiquetaOrigen;
    private javax.swing.JLabel etiquetaPrecio;
    private javax.swing.JLabel etiquetaRegistro;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner spinerFechaLlegada;
    private javax.swing.JSpinner spinerFechaSalida;
    private javax.swing.JSpinner spinerHoraLlegada;
    private javax.swing.JSpinner spinerHoraSalida;
    public javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables
}
