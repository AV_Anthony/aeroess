
package Vistas_GestionAerolinea;
import Vistas_Inició.VentanaPrincipal;
import Controladores_bd_sql.CifrarContraseña;
import Controladores_bd_sql.Conexion;
import Controladores_bd_sql.SQLAerolinea;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import modelo.Aerolinea;


public class VentanaDatosAerolinea extends javax.swing.JFrame {
    Aerolinea aerolinea = new Aerolinea();
    SQLAerolinea sqlaerolinea = new SQLAerolinea();
    File archivo;
    
   
    
    
    public VentanaDatosAerolinea() {
        initComponents();
        botonRealizarCambios.setVisible(false);
        desactivarCajas();
        setLocationRelativeTo(null);
        //rsscalelabel.RSScaleLabel.setScaleLabel(imgUsuario, "fb.jpg");
        
    }
    
     public VentanaDatosAerolinea(Aerolinea aerolinea) {
        initComponents();
        this.aerolinea = aerolinea;
        
        mostrarDatos();
        
        if(cargarImagen()>0){
            
        }else{
            rsscalelabel.RSScaleLabel.setScaleLabel(imgUsuario, "fb.jpg");
        }
            
        botonRealizarCambios.setVisible(false);
        desactivarCajas();
        setLocationRelativeTo(null);
    }
     
     public void mostrarDatos(){
         cajaNombreAerolinea.setText(aerolinea.getNombreAerolinea());
         cajaNombreEncargado.setText(aerolinea.getNombreEncargado());
         cajaApellidos.setText(aerolinea.getApellidos());
         cajaDni.setText(aerolinea.getDni());
         cajaCorreo.setText(aerolinea.getCorreo());
         cajaTelefono.setText(aerolinea.getTelefono());
     }
     
     public void limpiarCajas(){
         cajaNombreAerolinea.setText("");
         cajaNombreEncargado.setText("");
         cajaApellidos.setText("");
         cajaDni.setText("");
         cajaCorreo.setText("");
         cajaTelefono.setText("");
     }
     
     public void desactivarCajas(){
         cajaNombreAerolinea.setEnabled(false);
         cajaNombreEncargado.setEnabled(false);
         cajaApellidos.setEnabled(false);
         cajaTelefono.setEnabled(false);
         cajaDni.setEnabled(false);
         cajaCorreo.setEnabled(false);
         botonSeImagen.setEnabled(false);
        
     }
     public void activarCajas(){
         cajaNombreAerolinea.setEnabled(true);
         cajaNombreEncargado.setEnabled(true);
         cajaApellidos.setEnabled(true);
         cajaTelefono.setEnabled(true);
         cajaDni.setEnabled(true);
         cajaCorreo.setEnabled(true);
         botonSeImagen.setEnabled(true);
         
     }
     
     public int cargarImagen(){
        int idImagen=aerolinea.getId();
        PreparedStatement ps=null;
        ResultSet rs= null;
        int j =0;
        Conexion con = new Conexion();
            try{
                Connection conexion = con.getConnection();
                ps= conexion.prepareStatement("select imagen_usuario from aerolinea where id_aerolinea=?");
                ps.setInt(1, idImagen);
                rs= ps.executeQuery();
                
                BufferedImage buffing= null;
                while(rs.next()){
                    InputStream imgBinario = rs.getBinaryStream(1);
                    buffing = ImageIO.read(imgBinario);
                    
                    int ancho= imgUsuario.getWidth();
                    int alto = imgUsuario.getHeight();
                    
                    Imagen imagen = new Imagen(ancho, alto, buffing);
                    imgUsuario.add(imagen);
                    imgUsuario.repaint();
                   j++;
                }
                rs.close();
            }catch(Exception ex){
               System.out.println("Error"+ex);
            }
            
            System.out.println(j);
            return j;
     }
     

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        etiquetaTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        etiquetaDatos = new javax.swing.JLabel();
        etiquetaNombreAerolinea = new javax.swing.JLabel();
        etiquetaNombreEncargado = new javax.swing.JLabel();
        etiquetaApellidos = new javax.swing.JLabel();
        etiquetaTelefono = new javax.swing.JLabel();
        etiquetaDNI = new javax.swing.JLabel();
        etiquetaCorreo = new javax.swing.JLabel();
        cajaNombreAerolinea = new javax.swing.JTextField();
        cajaApellidos = new javax.swing.JTextField();
        cajaNombreEncargado = new javax.swing.JTextField();
        cajaTelefono = new javax.swing.JTextField();
        cajaDni = new javax.swing.JTextField();
        cajaCorreo = new javax.swing.JTextField();
        botonModificar = new javax.swing.JButton();
        botonRealizarCambios = new javax.swing.JButton();
        botonAtras = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        botonCambiarContraseña = new javax.swing.JButton();
        imgUsuario = new javax.swing.JLabel();
        botonSeImagen = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        etiquetaTitulo.setFont(new java.awt.Font("Arabic Typesetting", 1, 36)); // NOI18N
        etiquetaTitulo.setText("DATOS AEROLINEA");

        etiquetaDatos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDatos.setText("Datos");

        etiquetaNombreAerolinea.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombreAerolinea.setText("Nombre de la Aerolinea");

        etiquetaNombreEncargado.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombreEncargado.setText("Nombre del Encargado");

        etiquetaApellidos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaApellidos.setText("Apellidos");

        etiquetaTelefono.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaTelefono.setText("Teléfono");

        etiquetaDNI.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDNI.setText("DNI");

        etiquetaCorreo.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaCorreo.setText("Correo");

        cajaNombreAerolinea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaNombreAerolineaActionPerformed(evt);
            }
        });

        botonModificar.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonModificar.setText("Modificar");
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonRealizarCambios.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonRealizarCambios.setText("Realizar cambios");
        botonRealizarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRealizarCambiosActionPerformed(evt);
            }
        });

        botonAtras.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonAtras.setText("Atras");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        botonCambiarContraseña.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonCambiarContraseña.setText("Cambiar Contraseña");
        botonCambiarContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCambiarContraseñaActionPerformed(evt);
            }
        });

        imgUsuario.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        botonSeImagen.setText("Seleccionar Imagen");
        botonSeImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSeImagenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(etiquetaTitulo, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonAtras, javax.swing.GroupLayout.Alignment.LEADING))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaNombreEncargado)
                            .addComponent(etiquetaTelefono)
                            .addComponent(cajaNombreEncargado, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaCorreo)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(cajaTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(119, 119, 119))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(botonModificar)
                                        .addGap(31, 31, 31)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(etiquetaApellidos)
                                    .addComponent(etiquetaDNI)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(botonRealizarCambios)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(cajaDni, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cajaApellidos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 481, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 22, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(etiquetaDatos)
                            .addComponent(cajaNombreAerolinea, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                            .addComponent(etiquetaNombreAerolinea)
                            .addComponent(botonCambiarContraseña, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(imgUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                            .addComponent(botonSeImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(37, 37, 37))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(etiquetaTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(etiquetaDatos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(etiquetaNombreAerolinea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaNombreAerolinea, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49)
                        .addComponent(botonCambiarContraseña)
                        .addGap(51, 51, 51)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etiquetaNombreEncargado)
                            .addComponent(etiquetaApellidos))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cajaApellidos)
                            .addComponent(cajaNombreEncargado, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etiquetaTelefono)
                            .addComponent(etiquetaDNI))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cajaDni)
                            .addComponent(cajaTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(etiquetaCorreo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(botonModificar)
                            .addComponent(botonRealizarCambios)
                            .addComponent(botonAtras))
                        .addGap(32, 32, 32))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(imgUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonSeImagen)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cajaNombreAerolineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaNombreAerolineaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cajaNombreAerolineaActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
       
        
       botonRealizarCambios.setVisible(true);
       activarCajas();
       botonModificar.setEnabled(false);
       botonCambiarContraseña.setEnabled(false);
       
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonRealizarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRealizarCambiosActionPerformed
     if(cajaNombreAerolinea.getText().equals("")|| cajaNombreEncargado.getText().equals("") || cajaApellidos.getText().equals("") || cajaDni.getText().equals("") || cajaTelefono.getText().equals("")|| cajaCorreo.getText().equals("")){
         JOptionPane.showMessageDialog(null, "Rellene todos los campos porfavor");
     }else{
        aerolinea.setNombreAerolinea(cajaNombreAerolinea.getText());
        aerolinea.setNombreEncargado(cajaNombreEncargado.getText());
        aerolinea.setApellidos(cajaApellidos.getText());
        aerolinea.setDni(cajaDni.getText());
        aerolinea.setTelefono(cajaTelefono.getText());
        aerolinea.setCorreo(cajaCorreo.getText());
        
       try{
             FileInputStream archivoSeleccionado = new FileInputStream(archivo);
             aerolinea.setImagenSelect(archivoSeleccionado);
             aerolinea.setTamañoImagen((int)archivo.length());
             
       }catch(Exception ex){
           
       }
      
        //ejecutamos
        sqlaerolinea.modificarDatosAerolinea(aerolinea);
        limpiarCajas();
        mostrarDatos();
        botonRealizarCambios.setVisible(false);
        desactivarCajas();
        botonModificar.setEnabled(true); 
        botonCambiarContraseña.setEnabled(true);
        
       
         
     }
     
     
    }//GEN-LAST:event_botonRealizarCambiosActionPerformed

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed
    VentanaPrincipal v1 = new VentanaPrincipal(aerolinea);
    v1.setVisible(true);
    this.dispose();
        
    }//GEN-LAST:event_botonAtrasActionPerformed

    private void botonCambiarContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCambiarContraseñaActionPerformed
    VentanaModificarContraseña v1 = new VentanaModificarContraseña(aerolinea);
    v1.setVisible(true);
    
    }//GEN-LAST:event_botonCambiarContraseñaActionPerformed

    private void botonSeImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSeImagenActionPerformed
     
        JFileChooser escoger = new JFileChooser();
        escoger.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.png","png");
        FileNameExtensionFilter filtro2 = new FileNameExtensionFilter("*.jpg","jpg");
        escoger.setFileFilter(filtro);
        escoger.setFileFilter(filtro2);
        
        int seleccion = escoger.showOpenDialog(this);
        
        
            if(seleccion == JFileChooser.APPROVE_OPTION){ //SI EL USUARIO ACEPTO
                archivo = escoger.getSelectedFile();
                String ruta = archivo.getAbsolutePath();
                rsscalelabel.RSScaleLabel.setScaleLabel(imgUsuario, ruta);
                 
            }
    }//GEN-LAST:event_botonSeImagenActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaDatosAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaDatosAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaDatosAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaDatosAerolinea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaDatosAerolinea().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAtras;
    private javax.swing.JButton botonCambiarContraseña;
    private javax.swing.JButton botonModificar;
    private javax.swing.JButton botonRealizarCambios;
    private javax.swing.JButton botonSeImagen;
    private javax.swing.JTextField cajaApellidos;
    private javax.swing.JTextField cajaCorreo;
    private javax.swing.JTextField cajaDni;
    private javax.swing.JTextField cajaNombreAerolinea;
    private javax.swing.JTextField cajaNombreEncargado;
    private javax.swing.JTextField cajaTelefono;
    private javax.swing.JLabel etiquetaApellidos;
    private javax.swing.JLabel etiquetaCorreo;
    private javax.swing.JLabel etiquetaDNI;
    private javax.swing.JLabel etiquetaDatos;
    private javax.swing.JLabel etiquetaNombreAerolinea;
    private javax.swing.JLabel etiquetaNombreEncargado;
    private javax.swing.JLabel etiquetaTelefono;
    private javax.swing.JLabel etiquetaTitulo;
    private javax.swing.JLabel imgUsuario;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables
}
