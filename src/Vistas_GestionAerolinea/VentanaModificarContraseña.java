
package Vistas_GestionAerolinea;

import Controladores_bd_sql.CifrarContraseña;
import Controladores_bd_sql.SQLAerolinea;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import modelo.Aerolinea;

public class VentanaModificarContraseña extends javax.swing.JFrame {
    Aerolinea aerolinea = new Aerolinea();
    SQLAerolinea sqlaerolinea = new SQLAerolinea();
    
    public VentanaModificarContraseña() {
        initComponents();
        setLocationRelativeTo(null);
    }
    
    public VentanaModificarContraseña(Aerolinea aerolinea) {
        initComponents();
        this.aerolinea = aerolinea;
        setLocationRelativeTo(null);
        
    }
    
    public void limpiarCajas(){
        cajaContraseña.setText("");
        cajaConfirmarContraseña.setText("");
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cajaContraseña = new javax.swing.JPasswordField();
        cajaConfirmarContraseña = new javax.swing.JPasswordField();
        etiquetaConfirmarContraseña = new javax.swing.JLabel();
        etiquetaContraseña = new javax.swing.JLabel();
        checkMostrarContraseña = new javax.swing.JCheckBox();
        checkMostrarContraseñaCon = new javax.swing.JCheckBox();
        etiquetaTitulo = new javax.swing.JLabel();
        botonCambiarContraseña = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cambio de Contraseña");
        setResizable(false);

        cajaContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaContraseñaActionPerformed(evt);
            }
        });

        etiquetaConfirmarContraseña.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaConfirmarContraseña.setText("Confirmar Contraseña");

        etiquetaContraseña.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaContraseña.setText("Contraseña");

        checkMostrarContraseña.setText("Mostrar Contraseña");
        checkMostrarContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkMostrarContraseñaActionPerformed(evt);
            }
        });

        checkMostrarContraseñaCon.setText("Mostrar Contraseña");
        checkMostrarContraseñaCon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkMostrarContraseñaConActionPerformed(evt);
            }
        });

        etiquetaTitulo.setFont(new java.awt.Font("Arabic Typesetting", 1, 36)); // NOI18N
        etiquetaTitulo.setText("CAMBIAR CONTRASEÑA");

        botonCambiarContraseña.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        botonCambiarContraseña.setText("Modificar");
        botonCambiarContraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCambiarContraseñaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(71, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botonCambiarContraseña)
                        .addGap(184, 184, 184))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(etiquetaTitulo)
                        .addGap(54, 54, 54))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(checkMostrarContraseñaCon)
                            .addComponent(cajaConfirmarContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkMostrarContraseña)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cajaContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(etiquetaContraseña)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(etiquetaConfirmarContraseña)
                        .addGap(45, 45, 45))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaConfirmarContraseña)
                    .addComponent(etiquetaContraseña))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cajaConfirmarContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkMostrarContraseña)
                        .addGap(21, 21, 21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(checkMostrarContraseñaCon)
                        .addGap(18, 18, 18)))
                .addComponent(botonCambiarContraseña)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cajaContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaContraseñaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cajaContraseñaActionPerformed

    private void checkMostrarContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkMostrarContraseñaActionPerformed
        if(checkMostrarContraseña.isSelected()){
            cajaContraseña.setEchoChar((char)0);
        }else{
            cajaContraseña.setEchoChar('*');
        }

    }//GEN-LAST:event_checkMostrarContraseñaActionPerformed

    private void checkMostrarContraseñaConActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkMostrarContraseñaConActionPerformed
        if(checkMostrarContraseñaCon.isSelected()){
            cajaConfirmarContraseña.setEchoChar((char)0);
        }else{
            cajaConfirmarContraseña.setEchoChar('*');
        }
    }//GEN-LAST:event_checkMostrarContraseñaConActionPerformed

    private void botonCambiarContraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCambiarContraseñaActionPerformed
      String contraseña = new String(cajaContraseña.getPassword());
      String confirmarContraseña = new String(cajaConfirmarContraseña.getPassword());
      
      if(contraseña.equals("") || confirmarContraseña.equals("")){
          JOptionPane.showMessageDialog(null, "Rellene todos los campos porfavor");
      }else{
          if(contraseña.equals(confirmarContraseña)){
             String nuevaContraseña = CifrarContraseña.md5(contraseña);
             aerolinea.setContraseña(nuevaContraseña);
             sqlaerolinea.modificarContraseñaAerolinea(aerolinea);
             limpiarCajas();
             
          }else{
            JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden");
          }
      }
      
       
        
    }//GEN-LAST:event_botonCambiarContraseñaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaModificarContraseña.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaModificarContraseña.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaModificarContraseña.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaModificarContraseña.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
         try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
        } catch (ClassNotFoundException ex) {
            //  Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaModificarContraseña().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCambiarContraseña;
    private javax.swing.JPasswordField cajaConfirmarContraseña;
    private javax.swing.JPasswordField cajaContraseña;
    private javax.swing.JCheckBox checkMostrarContraseña;
    private javax.swing.JCheckBox checkMostrarContraseñaCon;
    private javax.swing.JLabel etiquetaConfirmarContraseña;
    private javax.swing.JLabel etiquetaContraseña;
    private javax.swing.JLabel etiquetaTitulo;
    // End of variables declaration//GEN-END:variables
}
