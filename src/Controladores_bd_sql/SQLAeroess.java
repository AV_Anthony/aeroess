
package Controladores_bd_sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import modelo.Aerolinea;
import modelo.Pasajero;
import modelo.Vuelo;



public class SQLAeroess {
    
    public boolean registrarAerolinea(Aerolinea aerolinea){
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        
   
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("insert into  aerolinea (nombre_aerolinea,contraseña,nombre_encargado,apellidos,dni,telefono,correo,id_tipousuario) values(?,?,?,?,?,?,?,?) ");
            ps.setString(1,aerolinea.getNombreAerolinea());
            ps.setString(2,aerolinea.getContraseña());
            ps.setString(3, aerolinea.getNombreEncargado());
            ps.setString(4, aerolinea.getApellidos());
            ps.setString(5, aerolinea.getDni());
            ps.setString(6, aerolinea.getTelefono());
            ps.setString(7, aerolinea.getCorreo());
            ps.setInt(8, aerolinea.getId_tipoUsuario());
            System.out.println(aerolinea.getNombreAerolinea()+"Nombresql:");
            ps.executeUpdate();
            
            return true;
                
                
            }catch(Exception ex){
                System.out.println(ex);
                return false;
            }
      
            
    }
    
    public int verificarAerolinea(String aerolinea){
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
   
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select count(id_aerolinea) from aerolinea where nombre_aerolinea=? ");
            ps.setString(1,aerolinea);
           
            rs = ps.executeQuery();
            
            if(rs.next()){
                return rs.getInt(1);
                
            }
            return 1;
                
            }catch(Exception ex){
                
                return 1;
            }
      
            
    }
    
    public boolean comprobarCorreo(String correo){
          //Patrón para validar el correo
         //PATRON QUE DEBE TENER UN CORREO ELECTRONICO
        Pattern patron = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"//COSAS QUE PUEDEN IR ANTES DEL @
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        
        Matcher matcher = patron.matcher(correo);
        
        return matcher.find();
        //METODO BOOLEAN QUE DEVUELVE TRUE SI EL CORREO CORRESPONDE CON EL PATRON   
    }

    
    
     public boolean iniciarSesion(Aerolinea aerolinea){
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
   
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select * from aerolinea where nombre_aerolinea=?");
            ps.setString(1,aerolinea.getNombreAerolinea());
           
            rs = ps.executeQuery();
            
                if(rs.next()){
                    if(aerolinea.getContraseña().equals(rs.getString("contraseña"))){
                        aerolinea.setId(rs.getInt("id_aerolinea"));
                        aerolinea.setNombreEncargado(rs.getString("nombre_encargado"));
                        aerolinea.setApellidos(rs.getString("apellidos"));
                        aerolinea.setDni(rs.getString("dni"));
                        aerolinea.setTelefono(rs.getString("telefono"));
                        aerolinea.setCorreo(rs.getString("correo"));
                        aerolinea.setId_tipoUsuario(rs.getInt("id_tipousuario"));
                        
                        return true;
                        
                 
                    }else{
                        System.out.println("fallo");   
                        return false;
                    }
                
                }
            System.out.println("no encuentra aerolinea");    
            return false; //SINO ENTRA AL IF
            
                }catch(Exception ex){
                    return false;
                }
      
            
    }
     
    
    public boolean registrarVuelo(Vuelo vuelo){
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        
   
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("insert into vuelo (numero_vuelo,origen,destino,fecha_salida,hora_salida,fecha_llegada,hora_llegada,numMax_pasajeros,precio,id_aerolineaFk) values(?,?,?,?,?,?,?,?,?,?) ");
            ps.setString(1,vuelo.getNumeroVuelo());
            ps.setString(2,vuelo.getOrigen());
            ps.setString(3,vuelo.getDestino());
            ps.setDate(4,Date.valueOf(vuelo.getFechaSalida())); 
            ps.setTime(5, Time.valueOf(vuelo.getHoraSalida()));
            ps.setDate(6,Date.valueOf(vuelo.getFechaLlegada())); //aca
            ps.setTime(7, Time.valueOf(vuelo.getHorsaLlegada()));
            ps.setInt(8,vuelo.getNumeroMaximoPasajeros());
            ps.setInt(9,vuelo.getPrecio());
            ps.setInt(10,vuelo.getId_aerolineaFk());
           
            ps.executeUpdate();
            
            return true;
                
                
            }catch(Exception ex){
                System.err.print(ex);
                return false;
            }
      
            
    }
    
    public boolean modificarVuelo(Vuelo vuelo){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("update vuelo set numero_vuelo=?,origen=?,destino=?,fecha_salida=?,hora_salida=?,fecha_llegada=?,hora_llegada=?,numMax_pasajeros=?,precio=? where id_vuelo=? ");
            ps.setString(1,vuelo.getNumeroVuelo());
            ps.setString(2,vuelo.getOrigen());
            ps.setString(3,vuelo.getDestino());
            ps.setDate(4,Date.valueOf(vuelo.getFechaSalida()));
            ps.setTime(5,Time.valueOf(vuelo.getHoraSalida()));
            ps.setDate(6,Date.valueOf(vuelo.getFechaLlegada()));
            ps.setTime(7,Time.valueOf(vuelo.getHorsaLlegada()));
            ps.setInt(8,vuelo.getNumeroMaximoPasajeros());
            ps.setInt(9,vuelo.getPrecio());
            ps.setInt(10,vuelo.getId());
           
            int resultado =ps.executeUpdate();
            
            
                if(resultado >0){
                     JOptionPane.showMessageDialog(null,"Registro actualizado correctamente");
                      return true;
                
                
                }else{
                     JOptionPane.showMessageDialog(null,"Error al actualizar registro");
                     return false;
                 }
            
            
            }catch(Exception ex){
                System.err.println("Error modificar vuelo"+ ex);
                return false;
                
            }
    }
    
    public boolean eliminarVuelo(Vuelo vuelo){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("delete from vuelo where id_vuelo=?");
            ps.setInt(1,vuelo.getId());
              
            int resultado =ps.executeUpdate();
            
            
                if(resultado >0){
                     JOptionPane.showMessageDialog(null,"Registro eliminado correctamente");
                     return true;
                
                
                }else{
                     JOptionPane.showMessageDialog(null,"Error al eliminar registro");
                     return false;
                 }
            
            
            }catch(Exception ex){
                System.err.println("Error"+ ex);
                return false;
                
            }
    }
    
    public Vuelo buscarVueloAerloniea(String numeroVuelo){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
            ResultSet rs = null;
            Vuelo vuelo = new Vuelo();
            
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select * from vuelo where numero_vuelo=? ");
            ps.setString(1, numeroVuelo);
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                vuelo.setId(rs.getInt("id_vuelo"));
                vuelo.setNumeroVuelo(rs.getString("numero_vuelo"));
                vuelo.setOrigen(rs.getString("origen"));
                vuelo.setDestino(rs.getString("destino"));
                vuelo.setFechaSalida(rs.getString("fecha_salida"));
                vuelo.setHoraSalida(rs.getString("hora_salida"));
                vuelo.setFechaLlegada(rs.getString("fecha_llegada"));
                vuelo.setHorsaLlegada(rs.getString("hora_llegada"));
                vuelo.setNumeroMaximoPasajeros(rs.getInt("numMax_pasajeros"));
                vuelo.setPrecio(rs.getInt("precio"));
                vuelo.setId_aerolineaFk(rs.getInt("id_aerolineaFk"));
                return vuelo;
               
                }else{
                     JOptionPane.showMessageDialog(null,"Registro no encontrado");
                     return null;
                 }

            }catch(Exception ex){
                System.err.println("Error al buscar vuelo"+ ex);
                return vuelo;
            }
    }
    
    
    public boolean compraVueloPasajero(Pasajero pasajero){
        Conexion con = new Conexion();
        PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("insert into pasajeros (nombre,apellidos,pasaporte,telefono,correo,id_vueloFK) values(?,?,?,?,?,?) ");
            ps.setString(1,pasajero.getNombre());
            ps.setString(2,pasajero.getApellidos());
            ps.setString(3, pasajero.getPasaporte());
            ps.setString(4, pasajero.getTelefono());
            ps.setString(5, pasajero.getCorreo());
            ps.setInt(6,pasajero.getIdVueloFk());
            
            ps.executeUpdate();
            
            return true;
                
            }catch(SQLException ex){
                System.out.println("Error al comprar el vuelo"+ex);
                return false;
            }
      
            
    }
    

}
