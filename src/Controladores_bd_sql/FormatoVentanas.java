
package Controladores_bd_sql;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class FormatoVentanas {
    
    
    
    public static void  FormatoVenTana( ){
        
        /*ESTILO
        Estilo de netBeans
            ESTILO1: javax.swing.plaf.metal.MetalLookAndFeel
            ESTILOS DESCARGADOS:
             //ESTILOS CLAROS 
            com.jtattoo.plaf.aero.AeroLookAndFeel  -->MEJOR
            com.jtattoo.plaf.acryl.AcryLookAndFeel
            //ESTILOS NEGROS
            com.jtattoo.plaf.graphite.GraphiteLookAndFeel
            
         */
        // -->asignacion de estilo
        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
        } catch (ClassNotFoundException ex) {
            //  Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            //Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
}
