
package Controladores_bd_sql;

import java.sql.Connection;
import java.sql.*;
import javax.swing.JOptionPane;
import modelo.Aerolinea;
import modelo.Vuelo;


public class SQLAerolinea {
    
    Aerolinea aerolinea= new Aerolinea();
    
    public boolean modificarDatosAerolinea(Aerolinea aerolinea){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("update aerolinea set nombre_aerolinea=?,nombre_encargado=?,apellidos=?,dni=?,telefono=?,correo=?,imagen_usuario=? where id_aerolinea=? ");
            ps.setString(1,aerolinea.getNombreAerolinea());
            ps.setString(2,aerolinea.getNombreEncargado());
            ps.setString(3,aerolinea.getApellidos());
            ps.setString(4,aerolinea.getDni());
            ps.setString(5,aerolinea.getTelefono());
            ps.setString(6,aerolinea.getCorreo());
            ps.setBinaryStream(7, aerolinea.getImagenSelect(),aerolinea.getTamañoImagen());
            ps.setInt(8,aerolinea.getId());
            
           
            int resultado =ps.executeUpdate();
            
            
                if(resultado >0){
                     JOptionPane.showMessageDialog(null,"Registro actualizado correctamente");
                      return true;
                
                
                }else{
                     JOptionPane.showMessageDialog(null,"Error al actualizar registro");
                     return false;
                 }

            }catch(Exception ex){
                System.err.println("Error"+ ex);
                return false;
            }
    }
    
    
    public boolean modificarContraseñaAerolinea(Aerolinea aerolinea){
            Conexion con = new Conexion();
            PreparedStatement ps = null;
        
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("update aerolinea set contraseña=? where id_aerolinea=?");
            ps.setString(1,aerolinea.getContraseña());
            ps.setInt(2,aerolinea.getId());

            int resultado =ps.executeUpdate();

                if(resultado >0){
                     JOptionPane.showMessageDialog(null,"Registro actualizado correctamente");
                      return true;

                }else{
                     JOptionPane.showMessageDialog(null,"Error al actualizar registro");
                     return false;
                 }

            }catch(Exception ex){
                System.err.println("Error"+ ex);
                return false;
            }
    }
    
 
    
}
