
package Vistas_Inició;

import Controladores_bd_sql.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import modelo.Aerolinea;

public class VentanaBusquedaDestino extends javax.swing.JFrame {
    
    Aerolinea aerolinea = new Aerolinea();
    SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
    DefaultTableModel modeloTabla= new DefaultTableModel();
    int id;
    
 
    public VentanaBusquedaDestino() {
        agregarModeloTabla();
        initComponents();
        cajaId.setVisible(false);
        setLocationRelativeTo(null);
        formatoJSpiner();
        botonComprarVuelo.setEnabled(false);
        
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(0);
      
    
    }
    
     public VentanaBusquedaDestino(Aerolinea aerolinea) {
        this.aerolinea = aerolinea;
        agregarModeloTabla();
        initComponents();
        cajaId.setVisible(false);
        formatoJSpiner();
        setLocationRelativeTo(null);
        botonComprarVuelo.setEnabled(false);
        
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(0);
       
    }
     
    public void formatoJSpiner(){
        
        JSpinner.DateEditor de1 = new JSpinner.DateEditor(spinerFechaSalida, "dd.MM.yyyy" );
        spinerFechaSalida.setEditor(de1);
       
        
    }
    
    
    
    private void agregarModeloTabla() {
        
        modeloTabla.addColumn("ID");
        modeloTabla.addColumn("Origen");
        modeloTabla.addColumn("Destino");
        modeloTabla.addColumn("Fecha Salida");
        modeloTabla.addColumn("Hora Salida");
        modeloTabla.addColumn("Fecha Llegada");
        modeloTabla.addColumn("Hora Llegada");
        modeloTabla.addColumn("Precio:€");
     
    }
    
    private void limpiarTabla(){
        for (int i = 0; i < tabla.getRowCount(); i++) {
        modeloTabla.removeRow(i);
        i-=1;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spinerFechaSalida = new javax.swing.JSpinner(new SpinnerDateModel());
        cajaOrigen = new javax.swing.JTextField();
        cajaDestino = new javax.swing.JTextField();
        etiquetaOrigen = new javax.swing.JLabel();
        etiquetaDestino = new javax.swing.JLabel();
        etiquetaTitulo = new javax.swing.JLabel();
        etiquetaFechaSalida = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        botonBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        botonComprarVuelo = new javax.swing.JButton();
        cajaId = new javax.swing.JTextField();
        botonAtras = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        etiquetaOrigen.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaOrigen.setText("Origen");

        etiquetaDestino.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDestino.setText("Destino");

        etiquetaTitulo.setFont(new java.awt.Font("Arial", 3, 24)); // NOI18N
        etiquetaTitulo.setText("Busqueda Vuelo");

        etiquetaFechaSalida.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaFechaSalida.setText("Fecha Salida");

        botonBuscar.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        botonBuscar.setText("Buscar");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        tabla.setModel(modeloTabla);
        ListSelectionListener oyenteSeleccion = new ListSelectionListener(){
            @Override

            public void valueChanged (ListSelectionEvent e){

                botonComprarVuelo.setEnabled(true);

                if(e.getValueIsAdjusting()){//

                    int filaSeleccionada = tabla.getSelectedRow();

                    id = (int)modeloTabla.getValueAt(filaSeleccionada, 0); 

                    String id1 = Integer.toString(id);

                    cajaId.setText(id1);  
                    botonComprarVuelo.setEnabled(true);
                }
            }
        };

        tabla.getSelectionModel().addListSelectionListener(oyenteSeleccion);
        jScrollPane1.setViewportView(tabla);

        botonComprarVuelo.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        botonComprarVuelo.setText("Comprar");
        botonComprarVuelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarVueloActionPerformed(evt);
            }
        });

        botonAtras.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        botonAtras.setText("Atras");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(etiquetaFechaSalida)
                            .addComponent(etiquetaOrigen))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cajaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(spinerFechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 136, Short.MAX_VALUE)
                        .addComponent(etiquetaDestino)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(36, 36, 36))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jScrollPane1)
                    .addComponent(botonComprarVuelo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(etiquetaTitulo))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(259, 259, 259)
                        .addComponent(botonBuscar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(botonAtras)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(etiquetaTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaOrigen)
                    .addComponent(cajaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaDestino)
                    .addComponent(cajaDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spinerFechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaFechaSalida))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(botonBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonComprarVuelo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
      
        String origen = cajaOrigen.getText();
        String destino = cajaDestino.getText();
        String fechaSalida = formater.format(spinerFechaSalida.getValue());
        
        
        
         Conexion con = new Conexion();
            PreparedStatement ps = null;
            ResultSet rs = null;
           
            
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select * from vuelo where origen=? AND destino=? AND fecha_salida=? ");
            ps.setString(1, origen);
            ps.setString(2, destino);
            ps.setDate(3, Date.valueOf(fechaSalida));
           
           
            
            rs = ps.executeQuery();
            
            if(rs.wasNull()){
                JOptionPane.showMessageDialog(null,"Registro no encontrado");
            }else{
               
                 limpiarTabla();
            }
           
            while(rs.next()){
              
               modeloTabla.addRow(new Object[]{rs.getInt("id_vuelo"),rs.getString("origen"),rs.getString("destino"),rs.getString("fecha_salida"),rs.getString("hora_salida"),rs.getString("fecha_llegada"),rs.getString("hora_llegada"),rs.getInt("precio") });

            }
                 
            }catch(Exception ex){
                System.err.println("Error al buscar vuelo compañia: "+ ex);
               
            }
        
    }//GEN-LAST:event_botonBuscarActionPerformed

    private void botonComprarVueloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarVueloActionPerformed
       
        VentanaCompra v1 = new VentanaCompra(id);
        v1.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_botonComprarVueloActionPerformed

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed
       
        VentanaPrincipal v1 =new  VentanaPrincipal(aerolinea);
        v1.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_botonAtrasActionPerformed

   
    public static void main(String args[]) {
     
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaBusquedaDestino.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaBusquedaDestino.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaBusquedaDestino.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaBusquedaDestino.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaBusquedaDestino().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAtras;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonComprarVuelo;
    private javax.swing.JTextField cajaDestino;
    private javax.swing.JTextField cajaId;
    private javax.swing.JTextField cajaOrigen;
    private javax.swing.JLabel etiquetaDestino;
    private javax.swing.JLabel etiquetaFechaSalida;
    private javax.swing.JLabel etiquetaOrigen;
    private javax.swing.JLabel etiquetaTitulo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner spinerFechaSalida;
    private javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables
}
