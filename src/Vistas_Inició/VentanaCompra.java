
package Vistas_Inició;

import Controladores_bd_sql.SQLAeroess;
import javax.swing.JOptionPane;
import modelo.Pasajero;

public class VentanaCompra extends javax.swing.JFrame {

    private int idVuelo;
    Pasajero pasajero = new Pasajero();
    SQLAeroess sql = new SQLAeroess();
    
    public VentanaCompra() {
        initComponents();
        setLocationRelativeTo(null);
    }
        
      public VentanaCompra(int idVuelo) {
        this.idVuelo = idVuelo;
        initComponents();
        setLocationRelativeTo(null);  
    }
    
      public void limpiarCajas(){
          cajaNombre.setText("");
          cajaApellidos.setText("");
          cajaPasaporte.setText("");
          cajaTelefono.setText("");
          cajaCorreo.setText("");
          
      }
      

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        etiquetaTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        etiquetaDatos = new javax.swing.JLabel();
        etiquetaNombre = new javax.swing.JLabel();
        etiquetaApellidos = new javax.swing.JLabel();
        etiquetaPasaporte = new javax.swing.JLabel();
        cajaNombre = new javax.swing.JTextField();
        cajaApellidos = new javax.swing.JTextField();
        cajaPasaporte = new javax.swing.JTextField();
        etiquetaNombre1 = new javax.swing.JLabel();
        cajaTelefono = new javax.swing.JTextField();
        etiquetaPasaporte1 = new javax.swing.JLabel();
        cajaCorreo = new javax.swing.JTextField();
        botonComprarVuelo = new javax.swing.JButton();
        botonAtras = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        etiquetaTitulo.setFont(new java.awt.Font("Arabic Typesetting", 1, 36)); // NOI18N
        etiquetaTitulo.setText("COMPRAR VUELO");

        etiquetaDatos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDatos.setText("Datos Cliente");

        etiquetaNombre.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombre.setText("Nombre ");

        etiquetaApellidos.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaApellidos.setText("Apellidos");

        etiquetaPasaporte.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaPasaporte.setText("Pasaporte");

        cajaNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        cajaApellidos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        cajaPasaporte.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        etiquetaNombre1.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaNombre1.setText("Telefono");

        etiquetaPasaporte1.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaPasaporte1.setText("Correo");

        botonComprarVuelo.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        botonComprarVuelo.setText("Comprar Vuelo");
        botonComprarVuelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarVueloActionPerformed(evt);
            }
        });

        botonAtras.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        botonAtras.setText("atrás");
        botonAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAtrasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(botonAtras)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(botonComprarVuelo, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(etiquetaTitulo)
                                .addComponent(etiquetaDatos)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(etiquetaNombre)
                                        .addComponent(cajaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(etiquetaPasaporte))
                                    .addGap(119, 119, 119)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(etiquetaNombre1)
                                        .addComponent(etiquetaApellidos)
                                        .addComponent(cajaApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(etiquetaPasaporte1)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(cajaPasaporte, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(117, 117, 117)
                                    .addComponent(cajaTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 58, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etiquetaDatos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaNombre)
                    .addComponent(etiquetaApellidos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cajaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaPasaporte)
                    .addComponent(etiquetaNombre1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cajaTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cajaPasaporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(etiquetaPasaporte1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cajaCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonComprarVuelo, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonAtras, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonComprarVueloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarVueloActionPerformed
        
        pasajero.setNombre(cajaNombre.getText());
        pasajero.setApellidos(cajaApellidos.getText());
        pasajero.setPasaporte(cajaPasaporte.getText());
        pasajero.setTelefono(cajaTelefono.getText());
        pasajero.setCorreo(cajaCorreo.getText());
        pasajero.setIdVueloFk(idVuelo);
        
        if (sql.compraVueloPasajero(pasajero)){
            JOptionPane.showMessageDialog(null, "Vuelo comprado correctamente");
        }else{
            JOptionPane.showMessageDialog(null, "Error al comprar el vuelo");
        }
        
        limpiarCajas();
        
    }//GEN-LAST:event_botonComprarVueloActionPerformed

    private void botonAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAtrasActionPerformed
       VentanaBusqCompañia v1 = new VentanaBusqCompañia();
       v1.setVisible(true);
       this.dispose();
        
        
    }//GEN-LAST:event_botonAtrasActionPerformed

    
    public static void main(String args[]) {
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaCompra().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAtras;
    private javax.swing.JButton botonComprarVuelo;
    private javax.swing.JTextField cajaApellidos;
    private javax.swing.JTextField cajaCorreo;
    private javax.swing.JTextField cajaNombre;
    private javax.swing.JTextField cajaPasaporte;
    private javax.swing.JTextField cajaTelefono;
    private javax.swing.JLabel etiquetaApellidos;
    private javax.swing.JLabel etiquetaDatos;
    private javax.swing.JLabel etiquetaNombre;
    private javax.swing.JLabel etiquetaNombre1;
    private javax.swing.JLabel etiquetaPasaporte;
    private javax.swing.JLabel etiquetaPasaporte1;
    private javax.swing.JLabel etiquetaTitulo;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
