
package Vistas_Inició;

import Controladores_bd_sql.Conexion;
import Controladores_bd_sql.SQLAeroess;
import java.awt.event.ItemEvent;
import java.sql.*;
import java.text.SimpleDateFormat;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import modelo.Aerolinea;
import modelo.Vuelo;


public class VentanaBusqCompañia extends javax.swing.JFrame {
    Vuelo vuelo1;
    SQLAeroess sql = new SQLAeroess();
    DefaultTableModel modeloTabla= new DefaultTableModel();
    int id;
    Aerolinea aerolinea = new Aerolinea();
    Aerolinea aerolineaCombo = new Aerolinea();
    SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
    
    public VentanaBusqCompañia() {
        modeloTabla();
        initComponents();
        botonBuscarFecha.setEnabled(false);
        botonComprarVuelo.setEnabled(false);
        cajaId.setVisible(false);
        
        //Agregar Lista de aerolineas
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel(aerolineaCombo.mostrarAerolineas());
        comboCompañias.setModel(modeloCombo);
        
        formatoJSpiner();
        
        setLocationRelativeTo(null);
        
        
        //OCULTAMOS LA COLUMNA ID
      
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(0);
        
        
    }
    
    public VentanaBusqCompañia(Aerolinea aerolinea){
        
        this.aerolinea = aerolinea;
        modeloTabla();
        initComponents();
        botonBuscarFecha.setEnabled(false);
        botonComprarVuelo.setEnabled(false);
        cajaId.setVisible(false);
        
        //Agregar Lista de aerolineas
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel(aerolineaCombo.mostrarAerolineas());
        comboCompañias.setModel(modeloCombo);
        
        formatoJSpiner();
        
        setLocationRelativeTo(null);
       
        //OCULTAMOS LA COLUMNA ID
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(0);
          
    }
    
     private void modeloTabla() {
        
        modeloTabla.addColumn("ID");
        modeloTabla.addColumn("Origen");
        modeloTabla.addColumn("Destino");
        modeloTabla.addColumn("Fecha Salida");
        modeloTabla.addColumn("Hora Salida");
        modeloTabla.addColumn("Fecha Llegada");
        modeloTabla.addColumn("Hora Llegada");
        modeloTabla.addColumn("Precio:€");
     
    }
    
    private void limpiarTabla(){
        for (int i = 0; i < tabla.getRowCount(); i++) {
        modeloTabla.removeRow(i);
        i-=1;
        }
    }
    
    
      public void formatoJSpiner(){
        
        JSpinner.DateEditor de1 = new JSpinner.DateEditor(spinerFechaSalida, "dd.MM.yyyy" );
        spinerFechaSalida.setEditor(de1);
    
        
      
        
    }

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboCompañias = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        botonComprarVuelo = new javax.swing.JButton();
        cajaId = new javax.swing.JTextField();
        botonBuscarFecha = new javax.swing.JButton();
        etiquetaOrigen = new javax.swing.JLabel();
        cajaOrigen = new javax.swing.JTextField();
        etiquetaFechaSalida = new javax.swing.JLabel();
        etiquetaDestino = new javax.swing.JLabel();
        cajaDestino = new javax.swing.JTextField();
        spinerFechaSalida = new javax.swing.JSpinner(new SpinnerDateModel());
        etiquetaTitulo = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        etiquetaOrigen1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        comboCompañias.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        comboCompañias.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboCompañiasItemStateChanged(evt);
            }
        });
        comboCompañias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCompañiasActionPerformed(evt);
            }
        });

        tabla.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        tabla.setModel(modeloTabla);
        ListSelectionListener oyenteSeleccion = new ListSelectionListener(){
            @Override

            public void valueChanged (ListSelectionEvent e){

                botonComprarVuelo.setEnabled(true);

                if(e.getValueIsAdjusting()){//

                    int filaSeleccionada = tabla.getSelectedRow();

                    id = (int)modeloTabla.getValueAt(filaSeleccionada, 0); 

                    String id1 = Integer.toString(id);

                    cajaId.setText(id1);   
                }
            }
        };
        tabla.getSelectionModel().addListSelectionListener(oyenteSeleccion);
        jScrollPane1.setViewportView(tabla);

        botonComprarVuelo.setText("Comprar");
        botonComprarVuelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarVueloActionPerformed(evt);
            }
        });

        botonBuscarFecha.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        botonBuscarFecha.setText("Buscar");
        botonBuscarFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarFechaActionPerformed(evt);
            }
        });

        etiquetaOrigen.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        etiquetaOrigen.setText("Origen");

        cajaOrigen.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        cajaOrigen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cajaOrigenActionPerformed(evt);
            }
        });

        etiquetaFechaSalida.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N
        etiquetaFechaSalida.setText("Fecha Salida");

        etiquetaDestino.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaDestino.setText("Destino");

        cajaDestino.setFont(new java.awt.Font("Arabic Typesetting", 0, 23)); // NOI18N

        spinerFechaSalida.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        etiquetaTitulo.setFont(new java.awt.Font("Arial", 3, 24)); // NOI18N
        etiquetaTitulo.setText("Busqueda por compañia");

        etiquetaOrigen1.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        etiquetaOrigen1.setText("Compañia");

        jButton1.setFont(new java.awt.Font("Arabic Typesetting", 0, 24)); // NOI18N
        jButton1.setText("Atras");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(etiquetaTitulo)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(etiquetaFechaSalida)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(spinerFechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(etiquetaDestino)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cajaDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(botonBuscarFecha)
                        .addGap(26, 26, 26))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonComprarVuelo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 577, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(etiquetaOrigen)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cajaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(etiquetaOrigen1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(comboCompañias, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboCompañias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaOrigen1)
                    .addComponent(cajaId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaOrigen)
                    .addComponent(cajaOrigen, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaDestino)
                    .addComponent(cajaDestino, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spinerFechaSalida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaFechaSalida)
                    .addComponent(botonBuscarFecha))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonComprarVuelo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboCompañiasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboCompañiasItemStateChanged
        
        if(evt.getStateChange() == ItemEvent.SELECTED){// alguno de mis aerollineas ha sido seleccionado?
            botonBuscarFecha.setEnabled(true);
            limpiarTabla();
            aerolineaCombo =(Aerolinea) comboCompañias.getSelectedItem();
            
            
            PreparedStatement ps = null;
            ResultSet rs =null;
            
            try{
            Conexion con = new Conexion();
            Connection conexion = con.getConnection();
            
            
            ps = conexion.prepareStatement("Select * from vuelo where id_aerolineaFk="+aerolineaCombo.getId());
            rs = ps.executeQuery();
            
            while(rs.next()){
                modeloTabla.addRow(new Object[]{rs.getInt("id_vuelo"), rs.getString("origen"), rs.getString("destino"), rs.getString("fecha_salida"),rs.getString("hora_salida"), rs.getString("fecha_llegada"),rs.getString("hora_llegada"), rs.getInt("precio")});
                
            }
            rs.close();
            
        }catch(SQLException ex){
            System.out.println("Error:" +ex);
        }
        }
        
        
        
        
    }//GEN-LAST:event_comboCompañiasItemStateChanged

    private void botonComprarVueloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarVueloActionPerformed
        
        VentanaCompra v1 = new VentanaCompra(id); //enviamos el id del vuelo
        v1.setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_botonComprarVueloActionPerformed

    private void cajaOrigenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cajaOrigenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cajaOrigenActionPerformed

    private void comboCompañiasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCompañiasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboCompañiasActionPerformed

    private void botonBuscarFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarFechaActionPerformed
        
        
        String origen = cajaOrigen.getText();
        String destino = cajaDestino.getText();
        String fechaSalida = formater.format(spinerFechaSalida.getValue());
        int idAerolinea = aerolineaCombo.getId();
        
        
         Conexion con = new Conexion();
            PreparedStatement ps = null;
            ResultSet rs = null;
           
            
            try{
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select * from vuelo where origen=? AND destino=? AND id_aerolineaFk=? AND fecha_salida=? ");
            ps.setString(1, origen);
            ps.setString(2, destino);
            ps.setInt(3, idAerolinea);
            ps.setDate(4, Date.valueOf(fechaSalida));
          
           
            
            rs = ps.executeQuery();
            
            if(rs.wasNull()){
                JOptionPane.showMessageDialog(null,"Registro no encontrado");
            }else{
               
                 limpiarTabla();
            }
           
            while(rs.next()){
              
               modeloTabla.addRow(new Object[]{rs.getInt("id_vuelo"),rs.getString("origen"),rs.getString("destino"),rs.getString("fecha_salida"),rs.getString("hora_salida"),rs.getString("fecha_llegada"),rs.getString("hora_llegada"),rs.getInt("precio") });

            }
                 
            }catch(Exception ex){
                System.err.println("Error al buscar vuelo compañia: "+ ex);
               
            }
        
        
    }//GEN-LAST:event_botonBuscarFechaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        VentanaPrincipal v1 = new VentanaPrincipal(aerolinea);
        v1.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

  
    public static void main(String args[]) {
      
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaBusqCompañia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaBusqCompañia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaBusqCompañia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaBusqCompañia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaBusqCompañia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonBuscarFecha;
    private javax.swing.JButton botonComprarVuelo;
    private javax.swing.JTextField cajaDestino;
    private javax.swing.JTextField cajaId;
    private javax.swing.JTextField cajaOrigen;
    private javax.swing.JComboBox<String> comboCompañias;
    private javax.swing.JLabel etiquetaDestino;
    private javax.swing.JLabel etiquetaFechaSalida;
    private javax.swing.JLabel etiquetaOrigen;
    private javax.swing.JLabel etiquetaOrigen1;
    private javax.swing.JLabel etiquetaTitulo;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSpinner spinerFechaSalida;
    private javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables
}
