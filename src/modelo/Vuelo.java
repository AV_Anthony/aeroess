
package modelo;


public class Vuelo {
    private int id;
    private String numeroVuelo;
    private String origen;
    private String destino;
    private String fechaSalida;
    private String horaSalida;
    private String fechaLlegada;
    private String horsaLlegada;
    int precio;
    int numeroMaximoPasajeros;
    int numActualPasajeros;
    int id_aerolineaFk;
    Pasajero listaPasajeros[];//F
    
    public Vuelo(){
        
    }

    public Vuelo(String numeroVuelo, String origen, String destino, String fechaSalida, String fechaLlegada, int precio, int numeroMaximoPasajeros,int id_aerolineaFk) {
        this.numeroVuelo = numeroVuelo;
        this.origen = origen;
        this.destino = destino;
        this.fechaSalida = fechaSalida;
        this.fechaLlegada = fechaLlegada;
        this.precio = precio;
        this.numeroMaximoPasajeros = numeroMaximoPasajeros;
        this.id_aerolineaFk = id_aerolineaFk;
    }

    public void setNumeroMaximoPasajeros(int numeroMaximoPasajeros) {
        this.numeroMaximoPasajeros = numeroMaximoPasajeros;
    }

    public String getNumeroVuelo() {
        return numeroVuelo;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public String getFechaLlegada() {
        return fechaLlegada;
    }

    public int getPrecio() {
        return precio;
    }

    public int getNumeroMaximoPasajeros() {
        return numeroMaximoPasajeros;
    }

    public int getNumActualPasajeros() {
        return numActualPasajeros;
    }

    public int getId() {
        return id;
    }

    public int getId_aerolineaFk() {
        return id_aerolineaFk;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public String getHorsaLlegada() {
        return horsaLlegada;
    }
    

    public Pasajero[] getListaPasajeros() {
        return listaPasajeros;
    }
    
    

    public void setNumeroVuelo(String numeroVuelo) {
        this.numeroVuelo = numeroVuelo;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public void setFechaLlegada(String fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public void setNumActualPasajeros(int numActualPasajeros) {
        this.numActualPasajeros = numActualPasajeros;
    }

    public void setListaPasajeros(Pasajero[] listaPasajeros) {
        this.listaPasajeros = listaPasajeros;
    }

    public void setId_aerolineaFk(int id_aerolineaFk) {
        this.id_aerolineaFk = id_aerolineaFk;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public void setHorsaLlegada(String horsaLlegada) {
        this.horsaLlegada = horsaLlegada;
    }
    
    
    
     
    
}
