
package modelo;


public class Pasajero {
    
    String nombre;
    String apellidos;
    String pasaporte;
    String telefono;
    String correo;
    int idVueloFk;

    
    //CONSTRUCTOR
    public Pasajero(){
        
    }
    
    public Pasajero(String nombre, String apellidos, String pasaporte, String telefono, String correo,int idVueloFk) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.pasaporte = pasaporte;
        this.telefono = telefono;
        this.correo = correo;
        this.idVueloFk = idVueloFk;
    }
    
    //GETERS

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getPasaporte() {
        return pasaporte;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public int getIdVueloFk() {
        return idVueloFk;
    }

   
    
    //Setters

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setPasaporte(String pasaporte) {
        this.pasaporte = pasaporte;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setIdVueloFk(int idVueloFk) {
        this.idVueloFk = idVueloFk;
    }

  
    
    

  
    
    
    
    
    
    
}
