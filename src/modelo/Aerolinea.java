
package modelo;

import Controladores_bd_sql.Conexion;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;


public class Aerolinea {
    private int id;
    private String nombreAerolinea;
    private String contraseña;
    private String nombreEncargado;
    private String apellidos;
    private String dni;
    private String telefono;
    private String correo;
    private int id_tipoUsuario;
    private FileInputStream imagenSelect;
    private int tamañoImagen;
   
    //CONSTRUCTORES
    public Aerolinea(){
        
    }
    
    
    public Aerolinea(String nombreAerolinea, String contraseña, String nombreEncargado, String apellidos, String dni, String telefono, String correo, int id_tipoUsuario) {
        this.nombreAerolinea = nombreAerolinea;
        this.contraseña = contraseña;
        this.nombreEncargado = nombreEncargado;
        this.apellidos = apellidos;
        this.dni = dni;
        this.telefono = telefono;
        this.correo = correo;
        this.id_tipoUsuario = id_tipoUsuario;
    }
    
    //METODOS GETERS
    
    public int getTamañoImagen() {
        return tamañoImagen;
    }

    public FileInputStream getImagenSelect() {
        return imagenSelect;
    }

    public String getNombreAerolinea() {
        return nombreAerolinea;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getNombreEncargado() {
        return nombreEncargado;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDni() {
        return dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public int getId_tipoUsuario() {
        return id_tipoUsuario;
    }

    public int getId() {
        return id;
    }
    
    //SETERS

    public void setTamañoImagen(int tamañoImagen) {
        this.tamañoImagen = tamañoImagen;
    }

    public void setNombreAerolinea(String nombreAerolinea) {
        this.nombreAerolinea = nombreAerolinea;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setNombreEncargado(String nombreEncargado) {
        this.nombreEncargado = nombreEncargado;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setId_tipoUsuario(int id_tipoUsuario) {
        this.id_tipoUsuario = id_tipoUsuario;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void insertarVuelo(){
        
    }

    public void setImagenSelect(FileInputStream imagenSelect) {
        this.imagenSelect = imagenSelect;
    }
    
     public String toString(){
        return this.nombreAerolinea;
    }
    
    public Vector<Aerolinea> mostrarAerolineas(){
        
        PreparedStatement ps = null;
        ResultSet rs =null;
        
        Vector<Aerolinea> vectorAerolineas =new Vector<Aerolinea>();
        Aerolinea aero= null;
        
        try{
            Conexion con = new Conexion();
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("Select * from aerolinea");
            rs = ps.executeQuery();
            
            aero = new Aerolinea();
            aero.setId(0);
            aero.setNombreAerolinea("Seleccione Aerolinea");
            vectorAerolineas.add(aero);
            
            while(rs.next()){
               
            aero = new Aerolinea();
            aero.setId(rs.getInt("id_aerolinea"));
            aero.setNombreAerolinea(rs.getString("nombre_aerolinea"));
            vectorAerolineas.add(aero);
            }
            rs.close();
            
        }catch(Exception ex){
            System.out.println("Error:" +ex);
        }
        vectorAerolineas.remove(1);
        return vectorAerolineas;
    }
    
    
}
